let  acceptCookies = () => {
    document.getElementById('popupContainer').style.display = 'none';

    localStorage.setItem(
        'cookieNecessary',
        document.getElementById("cookieNecessary").checked
    );
    localStorage.setItem(
        'cookiePreferences',
        document.getElementById("cookiePreferences").checked
    );
    localStorage.setItem(
        'cookieStatistics',
        document.getElementById("cookieStatistics").checked
    );
}

let  necessaryCookies = () => {
    document.getElementById('popupContainer').style.display = 'none';
    localStorage.setItem('cookieNecessary', 'true');
}


let hasConsentedToCookies = localStorage.getItem('cookieConsent');

if (!hasConsentedToCookies) {
    document.getElementById('popupContainer').style.display = 'block';
}